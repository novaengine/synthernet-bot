package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/tkanos/gonfig"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
	_ "github.com/joho/godotenv/autoload"
)

// Configuration struct
type Configuration struct {
	Port             int
	StaticVariable   string
	Token            string
	ConnectionString string
}

type PhotosynthernetConfig struct {
	ClientID     string
	ClientSecret string
	AccessToken  string
}

type PhotosynthernetLoginResponse struct {
	AccessToken string `json:"access_token"`
}

type PhotosynthernetImageResponse struct {
	Data []PhotosynthernetImage `json:"data"`
}

type PhotosynthernetImage struct {
	Id int `json:"id"`
	Attributes PhotosynthernetAttributes `json:"attributes"`
}

type PhotosynthernetUrl struct {
	Slug string `json:"slug"`
}

type PhotosynthernetAttributes struct {
	Title string `json:"title"`
	Urls []PhotosynthernetUrl `json:"url"`
}

var PConfig PhotosynthernetConfig = PhotosynthernetConfig{}

func main() {
	configuration := Configuration{}
	err := gonfig.GetConf("./config/options.json", &configuration)
	configuration.Token = os.Getenv("BOT_TOKEN")

	// Login to Photosynthernet
	PConfig = PhotosynthernetConfig{
		ClientID:     os.Getenv("PHOTO_ID"),
		ClientSecret: os.Getenv("PHOTO_SECRET"),
		AccessToken:  "",
	}

	postBody, _ := json.Marshal(map[string]string{
		"grant_type" : "client_credentials",
		"client_id" : PConfig.ClientID,
		"client_secret" : PConfig.ClientSecret,
	})
	responseBody := bytes.NewBuffer(postBody)

	resp, err := http.Post("https://photosynther.net/oauth/token", "application/json", responseBody)
	//Handle Error
	if err != nil {
		log.Fatalf("An Error Occured %v", err)
	}
	defer resp.Body.Close()
	//Read the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	loginResponse := PhotosynthernetLoginResponse{}
	json.Unmarshal(body, &loginResponse)

	PConfig.AccessToken = loginResponse.AccessToken
	

	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + configuration.Token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(messageCreate)

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	operator := "!"

	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}

	// Split message to find prefix
	messageSplit := strings.Split(m.Content, " ")

	command := messageSplit[0]

	switch command {

	case operator + "h", operator + "help":
		s.ChannelMessageSend(m.ChannelID, ``+"```"+`
	!h,!help            | View this help message
	!sb,!s [Message]    | Turns your message into SpOnGeBoB MeMe TeXt
	!ri,!r [Query]      | Searches Photosynthernet for an image matching your query.
                          Will return a random image from the resultset...
`+"```"+``)
	case operator + "sb", operator + "s":

		messageMinusPrefix := append(messageSplit[:0], messageSplit[1:]...)
		messageMinusPrefixString := strings.Join(messageMinusPrefix, " ")
		s.ChannelMessageDelete(m.ChannelID, m.ID)
		s.ChannelMessageSend(m.ChannelID, spongeBot(messageMinusPrefixString))

		break

	case operator + "ri", operator + "r":

		if len(messageSplit) <= 1 {
			s.ChannelMessageSend(m.ChannelID, "Please enter a query like '!ri Red' or !ri 'Mountain'")
			return
		}

		queryArr := append(messageSplit[:0], messageSplit[1:]...)
		query := strings.Join(queryArr, " ")

		req, err := http.NewRequest("GET", "https://photosynther.net/api/search?filter[query]=" + query, nil)
		req.Header.Set("Authorization", "Bearer " + PConfig.AccessToken)

		client := &http.Client{}
		resp, err := client.Do(req)

		if err != nil {
			log.Fatalln(err)
		}
		// We Read the response body on the line below.
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatalln(err)
		}

		fmt.Println(string(body))
		imageResponse := PhotosynthernetImageResponse{}
		r := bytes.NewReader(body)
		err = json.NewDecoder(r).Decode(&imageResponse)

		if err != nil {
			log.Println(err)
			return
		}
		fmt.Println(imageResponse)

		imagesReturned := len(imageResponse.Data)

		if imagesReturned == 0 {
			s.ChannelMessageSend(m.ChannelID, "No Images Found :(")
			return
		}

		randImgKey := rand.Intn(imagesReturned)
		randImg := imageResponse.Data[randImgKey].Attributes.Urls[0].Slug
		s.ChannelMessageSend(m.ChannelID, "https://photosynther.net/images/" + randImg)



	}


	// Everyone
	if rand.Intn(200) < 10 {
		s.ChannelMessageSend(m.ChannelID, spongeBot(m.Content))
	}
}

func spongeBot(args string) string {
	split := strings.Split(args, "")
	compiled := make([]string, 0)

	for i := range split {
		if rand.Intn(10) < 4 || i%2 == 0 {
			compiled = append(compiled, strings.ToUpper(split[i]))
		} else {
			compiled = append(compiled, split[i])
		}
	}

	output := strings.Join(compiled, "")

	return output
}

func spongeBotImage(text string) string {
	return "./images/base.png"
}
